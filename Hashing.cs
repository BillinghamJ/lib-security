﻿using System;
using System.Configuration;
using System.Security.Cryptography;

namespace Tam.Lib.Security
{
	/// <summary>
	/// Utilities for creating and validating hashed strings, often for passwords
	/// </summary>
  public static class Hashing
  {
    private static readonly RNGCryptoServiceProvider RNGCryptoServiceProvider;
    private static readonly int SaltBytes;
    private static readonly int HashBytes;
    private static readonly int PBKDF2Iterations;

    static Hashing()
    {
      RNGCryptoServiceProvider = new RNGCryptoServiceProvider();

			if (!int.TryParse(ConfigurationManager.AppSettings["Hashing_SaltBytes"], out SaltBytes)) SaltBytes = 24;
      if (!int.TryParse(ConfigurationManager.AppSettings["Hashing_HashBytes"], out HashBytes)) HashBytes = 24;
			if (!int.TryParse(ConfigurationManager.AppSettings["Hashing_PBKDF2Iterations"], out PBKDF2Iterations)) PBKDF2Iterations = 1000;
    }

		/// <summary>
		/// Generate a hash from the given input
		/// </summary>
		/// <param name="input">The input to hash</param>
		/// <returns>The secure hashed equivalent of the input</returns>
    public static string Create(string input)
    {
      var salt = new byte[SaltBytes];
      RNGCryptoServiceProvider.GetBytes(salt);

      var hash = PBKDF2(input, salt, PBKDF2Iterations, HashBytes);

      return string.Format("{0}:{1}:{2}", PBKDF2Iterations, Convert.ToBase64String(salt), Convert.ToBase64String(hash));
    }

		/// <summary>
		/// Checks the input against a stored hash
		/// </summary>
		/// <param name="input">The input to check</param>
		/// <param name="storedHash">The hash to check against</param>
		/// <returns>Whether the input and hash match</returns>
    public static bool Validate(string input, string storedHash)
    {
      try
      {
        var split = storedHash.Split(':');
        var iterations = Int32.Parse(split[0]);
        var salt = Convert.FromBase64String(split[1]);
        var hash = Convert.FromBase64String(split[2]);

        var testHash = PBKDF2(input, salt, iterations, hash.Length);

        return SlowEquals(hash, testHash);
      }
      catch
      {
        return false;
      }
    }

    private static bool SlowEquals(byte[] a, byte[] b)
    {
      var diff = (uint) a.Length ^ (uint) b.Length;

      for (var i = 0; i < a.Length && i < b.Length; i++)
        diff |= (uint) (a[i] ^ b[i]);

      return diff == 0;
    }

    private static byte[] PBKDF2(string input, byte[] salt, int iterations, int length)
    {
      return new Rfc2898DeriveBytes(input, salt, iterations).GetBytes(length);
    }
  }
}
